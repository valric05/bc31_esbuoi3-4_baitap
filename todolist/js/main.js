let myToDoList = ["Play game", "Soccer", "Apple", "Lemon", "Banana"];
let myCompleteList = ["Learn HTML", "SASS"];

const addToDoList = () => {
  let toDo = document.getElementById("newTask").value;
  myToDoList.push(toDo);
  renderHTML();
};

const renderHTML = () => {
  let toDoHTML = "";
  for (let i = 0; i < myToDoList.length; i++) {
    toDoHTML += `<li>${myToDoList[i]}
    <div class="buttons">
    <button class="remove" onclick="deleteWork(${i})"><i class="fa fa-trash-alt"></i></button><button class="complete" onclick="completeWork(${i})"><i class="fa fa-check"></i></button>
    </div>
    </li>`;
  }
  document.getElementById("todo").innerHTML = toDoHTML;

  let completeHTML = "";
  for (let i = 0; i < myCompleteList.length; i++) {
    completeHTML += `<li>${myCompleteList[i]}
    <div class="buttons">
    <button class="remove" onclick="deleteWorkComplete(${i})"><i class="fa fa-trash-alt"></i></button><button class="complete"><i class="fa fa-check-circle"></i></button>
    </div>
    </li>`;
  }
  document.getElementById("completed").innerHTML = completeHTML;
};

renderHTML();

const deleteWorkToDo = (index) => {
  myToDoList.splice(index, 1);
  renderHTML();
};

const deleteWorkComplete = (index) => {
  myCompleteList.splice(index, 1);
  renderHTML();
};

const completeWork = (index) => {
  let work = myToDoList.splice(index, 1);
  myCompleteList.push(work);
  renderHTML();
};

const sortAZ = () => {
  myToDoList.sort();
  renderHTML();
};

const sortZA = () => {
  myToDoList.sort();
  myToDoList.reverse();
  renderHTML();
};
